import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = ({ data }) => {
  const entry = data.craft.entries[0]
  console.log(entry)
  return (
    <Layout>
      <SEO title="Page two" />
      <h1>{entry.title}</h1>
      <p>{entry.h1Title}</p>
      <Link to="/">Go back to the homepage</Link>
    </Layout>
  )
}

export const query = graphql`
  query PageQuery {
    craft {
      entries {
        ... on Craft_Homepage {
          title
          h1Title
        }
      }
    }
  }
`

export default SecondPage
